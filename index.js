import express from "express"
import { Sequelize, DataTypes } from "sequelize"
import { fileURLToPath } from "url"
import path from "path"

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Initializing express server
const app = express();
app.use(express.json());

// Initializing database
const db = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(__dirname, 'database.sqlite')
})

// Function to set up the DB
async function initDB() {
    // Checking the DB
    await db.authenticate();

    // Defining a table inside the DB
    // Each attribute is set with a type and they cannot be null (empty)
    const People = db.define('people', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        surname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        age: {
            type: DataTypes.NUMBER,
            allowNull: false
        },
    })

    // Checking if the table exists
    // {force: true} forces the table to be deleted and created from scratch
    await People.sync({ force: true });

    let list = [
        {
            name: "John",
            surname: "Doe",
            age: 24
        },
        {
            name: "Susan",
            surname: "Miller",
            age: 29
        },
        {
            name: "Sam",
            surname: "Williams",
            age: 40
        }
    ]

    // Insert a list of element inside the DB
    await People.bulkCreate(list);

    return {
        People
    }
}

async function initAPI() {

    // Waiting for the DB to be ready
    const model = await initDB();

    // Simple endpoint that send a message when called
    app.get('/', (req, res) => {
        res.send("Everything's OK!").status(200);
    })

    // Endpoint that send back the value written in the query
    // Example: localhost:3000/query?test=something&number=20
    app.get('/query', (req, res) => {

        // req.query returns the object containing the values in the query
        let data = req.query;

        let msg = "You sent me these values:"
        for(let key in data) {
            msg += `{${key} = ${data[key]}}` 
        }

        res.status(200).send(msg);
    })

    // Endpoint that send back all the values stored in the database
    app.get('/db', async (req, res) => {
        let data = await model.People.findAll();
        res.send(data).status(200);
    })

    // Endpoint that saves a value in the database
    app.post('/db', async (req, res) => {
        // The data is stored in the body of the request
        let data = req.body;
        
        // try/catch is a construct that allows to try to do something. If it fails, the operation inside the catch are executed
        try {
            // Trying to save data in the DB
            await model.People.create(data);

            res.status(200).send()
        }
        catch {
            // Send back error in case something went wrong (for example, empty fields)
            res.status(400).send()
        }    
    })

    // The server listen port 3000
    app.listen(3000, () => {
        console.log("Listening on port 3000")
    })
    
}

// Executing code
initAPI()