# T03 - Backend

This is a similar version to what has been developed during the lecture on 17/03/2023.

## Install
The project is already set up with all the dependencies. Once downloaded/cloned, move in the folder using the terminal (or open the project using VSCode so that the terminal is already in the correct folder) and then call:

    npm install

This will ensure that all the dependencies are downloaded and available for the project.

When everything is ready, use:

    node index.js

This will start the server. The server listen on port 3000 and the following endpoint are available:

- / - Returns a simple message 
- /query - Accept query parameters and returns a list of all the parameters sent


        localhost:3000/query?param=something

- /db (get) - Returns all the values stored in the db
- /db (post) - Add a new record to the DB. The data must be passed in the body of the request.